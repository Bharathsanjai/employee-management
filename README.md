# README #

This README explains the steps to get employee management application up and running.

### What is this repository for? ###

* Capabilities to create and retrieve employees resources
* Version 0.1


### How do I get set up? ###

* Java 8 
* Maven 3.3
* Intellij or Eclipse
* H2 in memory
* Redis 6.2.1 Stand alone 
* mvn test to run unit tests
* mvn spring-boot:run to run the project

### Contribution guidelines ###

* Writing TDD tests 
* Code review coverage above 90%
* Design patterns
