package com.paypal.bfs.test.employeeserv.dto;

import lombok.Builder;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
@Data
@Builder
public class AddressDTO {
    @Id
    @GeneratedValue
    private Long id;
    private String line1;
    private String line2;
    private String city;
    private String state;
    private String country;
    private String zipcode;


    @OneToOne(mappedBy = "address")
    private EmployeeDTO employeeDTO;
}
