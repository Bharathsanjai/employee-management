package com.paypal.bfs.test.employeeserv.cache;

import com.paypal.bfs.test.employeeserv.dto.EmployeeDTO;
import org.springframework.stereotype.Service;

@Service
public interface MiddleWare {

    public void put(String key, EmployeeDTO employeeDTO);

    public EmployeeDTO getFromCache(String key);
}
