package com.paypal.bfs.test.employeeserv.mapper;

import com.paypal.bfs.test.employeeserv.api.model.Address;
import com.paypal.bfs.test.employeeserv.api.model.Employee;
import com.paypal.bfs.test.employeeserv.dto.AddressDTO;
import com.paypal.bfs.test.employeeserv.dto.EmployeeDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
public class EmployeeModelMapper {


    public Employee dtoToCore(EmployeeDTO employeeDTO) {
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeDTO, employee);
        employee.setAddress(dtoToCore(employeeDTO.getAddress()));
        return employee;
    }

    public EmployeeDTO coreToDto(Employee employee) {
        EmployeeDTO employeeDTO = EmployeeDTO.builder().build();
        BeanUtils.copyProperties(employee, employeeDTO);
        employeeDTO.setAddress(coreToDto(employee.getAddress()));
        return employeeDTO;
    }

    private AddressDTO coreToDto(Address address) {
        AddressDTO addressDTO = AddressDTO.builder().build();
        BeanUtils.copyProperties(address, addressDTO);
        return addressDTO;
    }

    private Address dtoToCore(AddressDTO addressDTO) {
        Address address = new Address();
        BeanUtils.copyProperties(addressDTO, address);
        return address;
    }


}
