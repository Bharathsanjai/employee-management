package com.paypal.bfs.test.employeeserv.cache;

import com.paypal.bfs.test.employeeserv.dto.EmployeeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

@Component
public class DefaultMiddleWare implements MiddleWare {

    @Autowired
    private RedisTemplate redisTemplate;

    public void put(String key, EmployeeDTO employeeDTO) {
        redisTemplate.opsForHash().put("employee", key, employeeDTO);
    }

    public EmployeeDTO getFromCache(String key) {
        return (EmployeeDTO) redisTemplate.opsForHash().get("employee", key);
    }
}
