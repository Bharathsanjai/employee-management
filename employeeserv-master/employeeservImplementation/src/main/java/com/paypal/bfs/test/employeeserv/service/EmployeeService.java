package com.paypal.bfs.test.employeeserv.service;

import com.paypal.bfs.test.employeeserv.api.model.Employee;
import com.paypal.bfs.test.employeeserv.cache.MiddleWare;
import com.paypal.bfs.test.employeeserv.dto.EmployeeDTO;
import com.paypal.bfs.test.employeeserv.exception.EmployeeNotFoundException;
import com.paypal.bfs.test.employeeserv.mapper.EmployeeModelMapper;
import com.paypal.bfs.test.employeeserv.repository.EmployeeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class EmployeeService {

    @Autowired
    EmployeeModelMapper employeeModelMapper;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private MiddleWare middleWare;

    public List<Employee> getAllEmployees() {
        List<Employee> employees = new ArrayList<>();
        employeeRepository
                .findAll()
                .forEach(employeeDTO -> employees.add(employeeModelMapper.dtoToCore(employeeDTO)));
        return employees;
    }

    public Employee getEmployeeById(int id) {

        EmployeeDTO employeeDTO = employeeRepository.findById(id)
                .orElseThrow(() -> new EmployeeNotFoundException("Employee doesn't exists"));

        return employeeModelMapper.dtoToCore(employeeDTO);
    }

    public EmployeeDTO saveOrUpdate(Employee employee, String idempotencyKey) {
        log.debug("Persisting employee {}", employee);

        EmployeeDTO fromCache = middleWare.getFromCache(idempotencyKey);

        if(fromCache != null) {
            log.debug("Avoiding re-creation for the key {}", idempotencyKey);
            return fromCache;
        }

        EmployeeDTO employeeDTO = Optional.of(employeeRepository.save(employeeModelMapper.coreToDto(employee)))
                .orElseThrow(() -> new EmployeeNotFoundException("Employee cannot be created"));

        middleWare.put(idempotencyKey, employeeDTO);

        return employeeDTO;

    }

    public void delete(int id) {
        log.debug("Removing employee with ID {}", id);
        employeeRepository.deleteById(id);
    }

}
