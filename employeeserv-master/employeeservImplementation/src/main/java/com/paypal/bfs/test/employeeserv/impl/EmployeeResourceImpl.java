package com.paypal.bfs.test.employeeserv.impl;

import com.paypal.bfs.test.employeeserv.api.EmployeeResource;
import com.paypal.bfs.test.employeeserv.api.model.Employee;
import com.paypal.bfs.test.employeeserv.dto.EmployeeDTO;
import com.paypal.bfs.test.employeeserv.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;


@RestController
@Slf4j
@Validated
public class EmployeeResourceImpl implements EmployeeResource {

    @Autowired
    EmployeeService employeeService;

    @Override
    public ResponseEntity<Employee> employeeGetById(@PathVariable("id") @DecimalMin("1") String id) {
        log.debug("Retreiving employee from DB for ID {}", id);
        Employee employee = employeeService.getEmployeeById(Integer.valueOf(id));
        return new ResponseEntity<>(employee, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Integer> addEmployee(@RequestHeader("Idempotency-Key") String idempotencyKey, @RequestBody @Valid Employee employee) {
        EmployeeDTO employeeDTO = employeeService.saveOrUpdate(employee, idempotencyKey);
        log.info("Added new employee for the request {}", employeeDTO);
        return new ResponseEntity<>(employeeDTO.getId(), HttpStatus.OK);
    }
}
