package com.paypal.bfs.test.employeeserv.service;

import com.paypal.bfs.test.employeeserv.api.model.Address;
import com.paypal.bfs.test.employeeserv.api.model.Employee;
import com.paypal.bfs.test.employeeserv.cache.MiddleWare;
import com.paypal.bfs.test.employeeserv.dto.AddressDTO;
import com.paypal.bfs.test.employeeserv.dto.EmployeeDTO;
import com.paypal.bfs.test.employeeserv.mapper.EmployeeModelMapper;
import com.paypal.bfs.test.employeeserv.repository.EmployeeRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import javax.swing.text.html.Option;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceTest {

    @InjectMocks
    EmployeeService employeeService;
    @Spy
    EmployeeModelMapper employeeModelMapper;
    @Mock
    private EmployeeRepository employeeRepository;
    @Mock
    private MiddleWare middleWare;

    @Test
    public void shouldSaveEmployee() {
        //Given
        Address address = new Address();
        address.setCity("BGLR");
        address.setLine1("NT");
        address.setState("KA");
        address.setCountry("IND");

        Employee employee = new Employee();
        employee.setId(1);
        employee.setDob(new Date());
        employee.setFirstName("paypal");
        employee.setLastName("bfs");
        employee.setAddress(address);

        EmployeeDTO employeeDTO = employeeModelMapper.coreToDto(employee);

        when(employeeRepository.save(any(EmployeeDTO.class))).thenReturn(employeeDTO);

        //When
        EmployeeDTO employeeDTOResponse = employeeService.saveOrUpdate(employee, "test");

        //Then
        verify(middleWare).getFromCache("test");
        verify(employeeRepository).save(any(EmployeeDTO.class));
        verify(middleWare).put(anyString(), any(EmployeeDTO.class));
        assertThat(employeeDTOResponse.getId(), is(employee.getId()));
        assertThat(employeeDTOResponse.getFirstName(), is(employee.getFirstName()));
        assertThat(employeeDTOResponse.getLastName(), is(employee.getLastName()));
        assertThat(employeeDTOResponse.getAddress().getLine1(), is(employee.getAddress().getLine1()));
        assertThat(employeeDTOResponse.getAddress().getState(), is(employee.getAddress().getState()));
        assertThat(employeeDTOResponse.getAddress().getCountry(), is(employee.getAddress().getCountry()));

    }

    @Test
    public void shouldReturnEmployeeFromMiddleware() {
        //Given
        Address address = new Address();
        address.setCity("BGLR");
        address.setLine1("NT");
        address.setState("KA");
        address.setCountry("IND");

        Employee employee = new Employee();
        employee.setId(1);
        employee.setDob(new Date());
        employee.setFirstName("paypal");
        employee.setLastName("bfs");
        employee.setAddress(address);

        EmployeeDTO employeeDTO = employeeModelMapper.coreToDto(employee);

        when(middleWare.getFromCache("test")).thenReturn(employeeDTO);


        //When
        EmployeeDTO employeeDTOResponse = employeeService.saveOrUpdate(employee, "test");

        //Then
        verify(middleWare).getFromCache("test");
        verify(employeeRepository, never()).save(any(EmployeeDTO.class));
        assertThat(employeeDTOResponse.getId(), is(employee.getId()));
        assertThat(employeeDTOResponse.getFirstName(), is(employee.getFirstName()));
        assertThat(employeeDTOResponse.getLastName(), is(employee.getLastName()));
        assertThat(employeeDTOResponse.getAddress().getLine1(), is(employee.getAddress().getLine1()));
        assertThat(employeeDTOResponse.getAddress().getState(), is(employee.getAddress().getState()));
        assertThat(employeeDTOResponse.getAddress().getCountry(), is(employee.getAddress().getCountry()));

    }

    @Test
    public void shouldReturnEmployeeIfPresent() {
        //Given
        AddressDTO address = AddressDTO.builder()
                .city("BGLR")
                .country("NT")
                .state("Ka")
                .line1("line1")
                .build();
        EmployeeDTO employeeDTO = EmployeeDTO.builder()
                .address(address)
                .firstName("paypal")
                .lastName("bfs")
                .id(1)
                .build();

        when(employeeRepository.findById(any(Integer.class))).thenReturn(Optional.ofNullable(employeeDTO));

        //When
        Employee employee = employeeService.getEmployeeById(1);

        //Then
        assertThat(employee.getId(), is(employeeDTO.getId()));
        assertThat(employee.getFirstName(), is(employeeDTO.getFirstName()));
        assertThat(employee.getLastName(), is(employeeDTO.getLastName()));
        assertThat(employee.getAddress().getCity(), is(employeeDTO.getAddress().getCity()));
        assertThat(employee.getAddress().getCountry(), is(employeeDTO.getAddress().getCountry()));
        assertThat(employee.getAddress().getState(), is(employeeDTO.getAddress().getState()));
        assertThat(employee.getAddress().getLine1(), is(employeeDTO.getAddress().getLine1()));
        assertThat(employee.getAddress().getLine2(), is(employeeDTO.getAddress().getLine2()));

    }

    @Test
    public void shouldReturnAllEmployees() {
        //Given
        Address address = new Address();
        address.setCity("BGLR");
        address.setLine1("NT");
        address.setState("KA");
        address.setCountry("IND");

        Employee employee = new Employee();
        employee.setId(1);
        employee.setDob(new Date());
        employee.setFirstName("paypal");
        employee.setLastName("bfs");
        employee.setAddress(address);

        Employee employee2 = new Employee();
        employee.setId(2);
        employee.setDob(new Date());
        employee.setFirstName("paypal");
        employee.setLastName("bfs");
        employee.setAddress(address);

        EmployeeDTO employeeDTO = employeeModelMapper.coreToDto(employee);
        EmployeeDTO employeeDTO2 = employeeModelMapper.coreToDto(employee);

        when(employeeRepository.findAll()).thenReturn(Arrays.asList(employeeDTO, employeeDTO2));

        //When
        List<Employee> allEmployees = employeeService.getAllEmployees();
        //Then

        assertThat(allEmployees.size(), is(2));
    }


    @Test
    public void shouldDeleteGivenEmployeeBasedOnId() {

        //When
        employeeService.delete(1);
        //Then
        verify(employeeRepository).deleteById(1);
    }
}
