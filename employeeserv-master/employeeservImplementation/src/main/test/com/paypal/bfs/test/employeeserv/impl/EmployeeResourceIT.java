package com.paypal.bfs.test.employeeserv.impl;

import com.paypal.bfs.test.employeeserv.EmployeeservApplication;
import com.paypal.bfs.test.employeeserv.api.model.Address;
import com.paypal.bfs.test.employeeserv.api.model.Employee;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;
import java.util.UUID;

import static org.hamcrest.Matchers.any;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EmployeeservApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EmployeeResourceIT {

    private TestRestTemplate restTemplate = new TestRestTemplate();
    private HttpHeaders headers = new HttpHeaders();
    private MockMvc mockMvc;

    @LocalServerPort
    private int port;
    @Autowired
    private WebApplicationContext wac;


    @Before
    public void setUp() throws Exception {
        mockMvc = webAppContextSetup(this.wac).build();
    }

    @Test
    public void testCreateEmployee() {

        //Given
        headers.add("Idempotency-Key", "unique-key2");
        HttpEntity<Employee> entity = new HttpEntity<Employee>(buildEmployee(), headers);

        //When
        ResponseEntity<Integer> response = restTemplate.exchange(
                createURLWithPort("/v1/bfs/employees"), HttpMethod.POST, entity, Integer.class);

        //Then
        assertThat(response.getBody(), any(Integer.class));

    }

    @Test
    public void testCreateEmployeeIdempotently() {

        //Given
        headers.add("Idempotency-Key", "unique-key1");
        HttpEntity<Employee> entity = new HttpEntity<Employee>(buildEmployee(), headers);

        //When
        ResponseEntity<Integer> response = restTemplate.exchange(
                createURLWithPort("/v1/bfs/employees"), HttpMethod.POST, entity, Integer.class);

        ResponseEntity<Integer> response2 = restTemplate.exchange(
                createURLWithPort("/v1/bfs/employees"), HttpMethod.POST, entity, Integer.class);

        //Then
        assertThat(response.getBody(), is(response2.getBody()));

    }

    @Test
    public void testRetreiveEmployee() {
        //Given
        headers.add("Idempotency-Key", UUID.randomUUID().toString());
        HttpEntity<Employee> entity = new HttpEntity<Employee>(buildEmployee(), headers);

        ResponseEntity<Integer> resource = restTemplate.exchange(
                createURLWithPort("/v1/bfs/employees"), HttpMethod.POST, entity, Integer.class);

        int id = resource.getBody();
        String uri = "/v1/bfs/employees/" + id;

        //When
        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort(uri), HttpMethod.GET, null, String.class);

        String expected = "{\"id\":1,\"first_name\":\"paypal\",\"last_name\":\"bfs\",\"address\":{\"line1\":\"NT\",\"city\":\"BGLR\",\"state\":\"KA\",\"country\":\"IND\",\"zipcode\":\"56\"}}";

        //Then
        assertThat(response.getBody(), is(expected));

    }

    @Test
    public void testEmployeeNotFoundException() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.get("/v1/bfs/employees/3"))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message", is("Employee doesn't exists")));
    }

    @Test
    public void testUnprocessableEntityWhenAddressIsInComplete() throws Exception {

        String employeeJson = "{\n" +
                "    \"title\": \"Employee resource\",\n" +
                "    \"description\": \"Employee resource object\",\n" +
                "    \"first_name\": \"one\",\n" +
                "    \"last_name\": \"two\",\n" +
                "    \"dob\": \"2020-10-10T00:00:00.00Z\",\n" +
                "    \"address\": {\n" +
                "        \"line1\": \"NT\",\n" +
                "        \"city\": \"BNG\",\n" +
                "        \"state\": \"KA\",\n" +
                "        \"country\": \"IND\"\n" +
                "        \n" +
                "    }\n" +
                "}";

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/v1/bfs/employees")
                .accept(MediaType.APPLICATION_JSON)
                .header("Idempotency-Key", UUID.randomUUID().toString())
                .content(employeeJson)
                .contentType(MediaType.APPLICATION_JSON);


       mockMvc.perform(requestBuilder)
               .andExpect(status().isUnprocessableEntity());

    }

    @Test
    public void testBadRequest() throws Exception {

        String employeeJson = "{\n" +
                "    \"title\": \"Employee resource\",\n" +
                "    \"description\": \"Employee resource object\",\n" +
                "    \"first_name\": \"one\",\n" +
                "    \"last_name\": \"two\",\n" +
                "    \"dob\": \"2020-10-10T00:00:00.00Z\",\n" +
                "    \"address\": {\n" +
                "        \"line1\": \"NT\",\n" +
                "        \"city\": \"BNG\",\n" +
                "        \"state\": \"KA\",\n" +
                "        \"country\": \"IND\"\n" +
                "        \n" +
                "    }\n" +
                "}";

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/v1/bfs/employees")
                .accept(MediaType.APPLICATION_JSON)
                .content(employeeJson)
                .contentType(MediaType.APPLICATION_JSON);


        mockMvc.perform(requestBuilder)
                .andExpect(status().isBadRequest());

    }

    private Employee buildEmployee() {
        Address address = new Address();
        address.setCity("BGLR");
        address.setLine1("NT");
        address.setState("KA");
        address.setCountry("IND");
        address.setZipcode("56");

        Employee employee = new Employee();
        employee.setDob(new Date());
        employee.setFirstName("paypal");
        employee.setLastName("bfs");
        employee.setAddress(address);

        return employee;
    }


    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }

}
