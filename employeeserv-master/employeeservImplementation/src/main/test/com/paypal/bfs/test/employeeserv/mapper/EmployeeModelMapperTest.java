package com.paypal.bfs.test.employeeserv.mapper;

import com.paypal.bfs.test.employeeserv.api.model.Address;
import com.paypal.bfs.test.employeeserv.api.model.Employee;
import com.paypal.bfs.test.employeeserv.dto.AddressDTO;
import com.paypal.bfs.test.employeeserv.dto.EmployeeDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Date;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeModelMapperTest {

    @InjectMocks
    EmployeeModelMapper employeeModelMapper;

    @Test
    public void shouldConvertEmployeeToDto() {
        //Given
        Address address = new Address();
        address.setCity("BGLR");
        address.setLine1("NT");
        address.setState("KA");
        address.setCountry("IND");

        Employee employee = new Employee();
        employee.setId(1);
        employee.setDob(new Date());
        employee.setFirstName("paypal");
        employee.setLastName("bfs");
        employee.setAddress(address);

        //When
        EmployeeDTO employeeDTO = employeeModelMapper.coreToDto(employee);

        //Then
        assertThat(employeeDTO.getId(), is(employee.getId()));
        assertThat(employeeDTO.getFirstName(), is(employee.getFirstName()));
        assertThat(employeeDTO.getLastName(), is(employee.getLastName()));
        assertThat(employeeDTO.getAddress().getCity(), is(employee.getAddress().getCity()));
        assertThat(employeeDTO.getAddress().getCountry(), is(employee.getAddress().getCountry()));
        assertThat(employeeDTO.getAddress().getState(), is(employee.getAddress().getState()));
        assertThat(employeeDTO.getAddress().getLine1(), is(employee.getAddress().getLine1()));
        assertThat(employeeDTO.getAddress().getLine2(), is(employee.getAddress().getLine2()));
    }

    @Test
    public void shouldConvertDtoToEmployee() {
        //Given
        AddressDTO address = AddressDTO.builder()
                .city("BGLR")
                .country("NT")
                .state("Ka")
                .line1("line1")
                .build();
        EmployeeDTO employeeDTO = EmployeeDTO.builder()
                .address(address)
                .firstName("paypal")
                .lastName("bfs")
                .id(1)
                .build();


        //When
        Employee employee = employeeModelMapper.dtoToCore(employeeDTO);

        //Then
        assertThat(employee.getId(), is(employeeDTO.getId()));
        assertThat(employee.getFirstName(), is(employeeDTO.getFirstName()));
        assertThat(employee.getLastName(), is(employeeDTO.getLastName()));
        assertThat(employee.getAddress().getCity(), is(employeeDTO.getAddress().getCity()));
        assertThat(employee.getAddress().getCountry(), is(employeeDTO.getAddress().getCountry()));
        assertThat(employee.getAddress().getState(), is(employeeDTO.getAddress().getState()));
        assertThat(employee.getAddress().getLine1(), is(employeeDTO.getAddress().getLine1()));
        assertThat(employee.getAddress().getLine2(), is(employeeDTO.getAddress().getLine2()));
    }


}
