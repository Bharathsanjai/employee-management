package com.paypal.bfs.bdd.stepdefs;

import com.paypal.bfs.test.employeeserv.api.model.Address;
import com.paypal.bfs.test.employeeserv.api.model.Employee;
import cucumber.api.java8.En;
import io.cucumber.datatable.DataTable;
import io.restassured.response.Response;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

/**
 * Step Definition Class for Employee.
 *
 * <p>Uses Java Lambda style step definitions so that we don't need to worry
 * about method naming for each step definition</p>
 */
public class EmployeeSteps extends AbstractSteps implements En {

    public EmployeeSteps() {

        Given("user wants to create an employee with the following attributes", (DataTable employeeDt) -> {
            testContext().reset();
            List<Employee> employeeList = employeeDt.asList(Employee.class);

            // First row of DataTable has the employee attributes hence calling get(0) method.
            super.testContext()
                    .setPayload(employeeList.get(0));
        });


        And("with the following address", (DataTable addresses) -> {
            List<Address> addressList = addresses.asList(Address.class);
            Employee employee = super.testContext()
                    .getPayload(Employee.class);
            Address address = addressList.get(0);
            employee.setAddress(address);
        });

        When("user saves the new employee {string}", (String testContext) -> {
            String createEmployeeUrl = "/v1/bfs/employees/";

            // AbstractSteps class makes the POST call and stores response in TestContext
            executePost(createEmployeeUrl);
        });

        Then("the save {string}", (String expectedResult) -> {
            Response response = testContext().getResponse();

            switch (expectedResult) {
                case "IS SUCCESSFUL":
                    assertThat(response.statusCode()).isIn(200, 201);
                    break;
                case "FAILS":
                    assertThat(response.statusCode()).isBetween(400, 504);
                    break;
                default:
                    fail("Unexpected error");
            }
        });

    }

}
