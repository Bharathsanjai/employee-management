Feature: Create Employee

  Scenario: WITH ALL REQUIRED FIELDS IS SUCCESSFUL

    Given user wants to create an employee with the following attributes
      | id  | first_name | last_name | dob        |
      | 100 | Rachel     | Green     | 1990-01-01 |

    And with the following address
      | line1 | line2  | city | state | country | zipcode |
      | 102   | Mobile | BGR  | KA    | IND     | 560075  |

    When user saves the new employee 'WITH ALL REQUIRED FIELDS'
    Then the save 'IS SUCCESSFUL'