package com.paypal.bfs.test.employeeserv.api;

import com.paypal.bfs.test.employeeserv.api.model.Employee;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;

/**
 * Interface for employee resource operations.
 */
public interface EmployeeResource {

    /**
     * Retrieves the {@link Employee} resource by id.
     *
     * @param id employee id.
     * @return {@link Employee} resource.
     */
    @RequestMapping("/v1/bfs/employees/{id}")
    ResponseEntity<Employee> employeeGetById(@PathVariable("id") @DecimalMin("1") String id);

    /**
     * Creates the {@link Employee} resource post vlaidating the payloaad.
     *
     * @param employee is the attributes for the employee.
     * @return {@link} of the resource.
     */
    @PostMapping("/v1/bfs/employees")
    ResponseEntity<Integer> addEmployee(@RequestHeader("Idempotency-Key") String idempotencyKey, @RequestBody @Valid Employee employee);
}
